Project Gutenber to Wikisource (an importing system)
====================================================

This tool is used to import books from Project Gutenber to Wikisource.
It splits books back into pages, matching the original scanned version.

For this tool to work, you must first

1. find a scan of the original edition used to produce the PG edition;
2. upload this to Commons, and create an Index page for it on Wikisource;
3. create a Wikidata item for the book, and give it at least a
   [Project Gutenberg ebook ID (P2034)](https://www.wikidata.org/wiki/Property:P2034)
   and a [Wikisource index page (P1957)](https://www.wikidata.org/wiki/Property:P1957).

Then, browse to `https://tools.wmflabs.org/pg2ws/[Index_page_name.pdf]`, log in,
and start splitting the original file to match the scan.

SELECT ?pgId ?wikisourceIndex
WHERE
{
  ?edition wdt:P2034 ?pgId .
  ?edition wdt:P1957 ?wikisourceIndex .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
}

