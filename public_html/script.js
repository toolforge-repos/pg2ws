$(document).ready(function () {

	/**
	 * Text-mover button, that moves the current selection from the #pgtxt textarea to the #wstxt one.
	 */
	$("#text-mover").on("click", function () {
		var selectedText, remainingText, pgTxt, selection, startPos, endPos;

		pgTxt = $('#pgtxt')[0];
		// Get selected text (IE version).
		if (document.selection != undefined) {
			$('#pgtxt').focus();
			selection = document.selection.createRange();
			selectedText = selection.text;
		}
		// Get selected text (Mozilla version).
		else if (pgTxt.selectionStart != undefined) {
			startPos = pgTxt.selectionStart;
			endPos = pgTxt.selectionEnd;
			selectedText = pgTxt.value.substring(startPos, endPos)
			remainingText = pgTxt.value.substring(0, startPos) + pgTxt.value.substring(endPos);
		}

		// Update the two textareas.
		$('#wstxt').val(selectedText);
		$('#pgtxt').val(remainingText);
	});

	/**
	 * Clean up HTML, turn it (sort of) into wikitext.
	 */
	$("#clean-up").on("click", function () {
		var oldTxt, newTxt;

		wstxt = $("#wstxt").val();

		newTxt = wstxt

			// Remove unwanted tags.
			.replace(/<\/?[pP]>/g, "")

			// Fix entities.
			.replace(/&mdash;/g, '—')
			.replace(/&ndash;/g, '–')
			.replace(/&quot;/g, '"')

			// Fix multiple line feeds.
			.replace(/\n\n+/g, "\n\n")

			// Remove single line feeds.
			.replace(/([^\n])\n([^\n])/g, "$1 $2")

			// Collapse multiple spaces into one.
			.replace(/  +/g, ' ')

			// Remove leading and trailing whitespace.
			.trim();

		$("#wstxt").val(newTxt);
	});

	/**
	 * Text box height.
	 */
	$("textarea").each(function() {
		$(this).outerHeight($(window).height() * 0.85);
	});

});
