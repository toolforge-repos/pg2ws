<?php

require __DIR__ . '/../vendor/autoload.php';

$app = new \Wikisource\Pg2ws\App( __DIR__ . '/..' );
$app->run();
