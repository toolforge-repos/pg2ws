<?php

namespace Wikisource\Pg2ws;

use MediaWiki\OAuthClient\Client;
use MediaWiki\OAuthClient\ClientConfig;
use MediaWiki\OAuthClient\Consumer;
use MediaWiki\OAuthClient\Token;
use Slim\Helper\Set;
use Slim\Slim;
use Slim\Views\TwigExtension;
use Stash\Driver\FileSystem;
use Stash\Pool;
use Wikimedia\Slimapp\AbstractApp;
use Wikimedia\Slimapp\Config;
use Wikisource\Pg2ws\Controllers\AuthController;
use Wikisource\Pg2ws\Controllers\HomeController;
use Wikisource\Pg2ws\Controllers\TransferController;

class App extends AbstractApp
{
    public function __construct($deployDir, $settings = [])
    {
        if (is_readable($deployDir . '/.env')) {
            Config::load($deployDir . '/.env');
        }
        $settings = array_merge($settings, [
            'debug' => Config::getBool('DEBUG', false),
            'oauth.enable' => true,
            'oauth.consumer_token' => Config::getStr('OAUTH_CONSUMER_TOKEN'),
            'oauth.secret_token' => Config::getStr('OAUTH_SECRET_TOKEN'),
            'oauth.endpoint' => Config::getStr('OAUTH_ENDPOINT'),
            'oauth.redir' => Config::getStr('OAUTH_REDIR'),
            'oauth.callback' => Config::getStr('OAUTH_CALLBACK'),
        ]);
        parent::__construct($deployDir, $settings);
    }

    /**
     * Apply settings to the Slim application.
     *
     * @param \Slim\Slim $slim Application
     */
    protected function configureSlim(Slim $slim)
    {
        ;
    }

    /**
     * Configure inversion of control/dependency injection container.
     *
     * @param \Slim\Helper\Set $container IOC container
     */
    protected function configureIoc(Set $container)
    {
        // OAuth Config.
        $container->singleton('oauthConfig', function ($c) {
            $conf = new ClientConfig($c->settings['oauth.endpoint']);
            $conf->setRedirUrl($c->settings['oauth.redir']);
            $consumer = new Consumer($c->settings['oauth.consumer_token'], $c->settings['oauth.secret_token']);
            $conf->setConsumer($consumer);
            return $conf;
        });
        // OAuth Client.
        $container->singleton('oauthClient', function ($c) {
            $client = new Client($c->oauthConfig, $c->log);
            $client->setCallback($c->settings['oauth.callback']);
            return $client;
        });
        // Cache.
        $container->singleton('cache', function ($c) {
            $cacheDir = $this->deployDir.'/cache';
            if (!is_dir($cacheDir)) {
                mkdir($cacheDir);
            }
            return new Pool(new FileSystem(['path'=>$cacheDir]));
        });
    }

    /**
     * Configure view behavior.
     *
     * @param \Slim\View $view Default view
     */
    protected function configureView(\Slim\View $view)
    {
        $view->replace(['app' => $this->slim,]);
        $view->parserExtensions = [ new TwigExtension() ];
    }

    /**
     * Configure routes to be handled by application.
     *
     * @param \Slim\Slim $slim Application
     */
    protected function configureRoutes(Slim $slim)
    {
        $userMiddleware = function () use ($slim) {
            if (isset($_SESSION[AuthController::ACCESS_TOKEN_KEY])) {
                $token = new Token($_SESSION[AuthController::ACCESS_TOKEN_KEY], $_SESSION[AuthController::ACCESS_TOKEN_SECRET]);
                $slim->container->set('user', $slim->container->oauthClient->identify($token));
                $slim->view->set('user', $this->slim->user);
            }
        };

        // Homepage.
        $slim->get('/', $userMiddleware, function () use ($slim) {
            $pages = new HomeController($slim);
            $pages();
        })->name('home');

        // Main transfer page.
        $slim->any('/transfer', $userMiddleware, function () use ($slim) {
            $cont = new TransferController($slim);
            $cont();
        })->name('transfer');

        // OAuth routes.
        $slim->group('/oauth/',
            function () use ($slim) {
                $slim->get('', function () use ($slim) {
                    $page = new AuthController($slim);
                    $page->setOAuth($slim->oauthClient);
                    $page('init');
                })->name('oauth_init');
                $slim->get('callback', function () use ($slim) {
                    $page = new AuthController($slim);
                    $page->setOAuth($slim->oauthClient);
                    //$page->setUserManager($slim->userManager);
                    $page('callback');
                })->name('oauth_callback');
            });
    }

    /**
     * Configure the default HeaderMiddleware installed for all routes.
     *
     * Default configuration adds these headers:
     * - "Vary: Cookie" to help upstream caches to the right thing
     * - "X-Frame-Options: DENY"
     * - A fairly strict 'self' only Content-Security-Policy to help protect
     *   against XSS attacks
     * - "Content-Type: text/html; charset=UTF-8"
     *
     * @return array
     */
    protected function configureHeaderMiddleware()
    {
        // Add headers to all responses:
        return array(
            'Vary' => 'Cookie',
            'X-Frame-Options' => 'DENY',
            'Content-Security-Policy' =>
                "default-src 'self' tools-static.wmflabs.org/cdnjs/ ; " .
                "child-src 'none'; " .
                "object-src 'none'; " .
                "img-src 'self' data: upload.wikimedia.org; " .
                "style-src 'self' 'unsafe-inline' tools-static.wmflabs.org/cdnjs/ ;",
            'Content-Type' => 'text/html; charset=UTF-8',
        );
    }
}
