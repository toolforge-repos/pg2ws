<?php

namespace Wikisource\Pg2ws\Controllers;

use Wikimedia\Slimapp\Controller;
use Wikimedia\Slimapp\Auth\AuthManager;
use MediaWiki\OAuthClient\Client;
use MediaWiki\OAuthClient\Token;

class AuthController extends Controller
{

	const REQEST_KEY = 'oauthreqtoken';
	const ACCESS_TOKEN_KEY = 'accesstokenkey';
	const ACCESS_TOKEN_SECRET = 'accesstokensecret';

	/**
	 * @var Client $oauth
	 */
	protected $oauth;

	/**
	 * @var OAuthUserManager $manager
	 */
	protected $manager;

	public function setOAuth( Client $oauth ) {
		$this->oauth = $oauth;
	}

	public function setUserManager( OAuthUserManager $manager ) {
		$this->manager = $manager;
	}

	protected function handleGet( $stage ) {

		// The $_GET check is only required because I stuffed up the callback registered at Meta, so things are redirected back to /oauth instead of /oauth/callback
		if ( $stage == 'callback' || isset( $_GET['oauth_verifier'] ) ) {
			$this->handleCallback();
		} else {
			$this->handleInitiate();
		}
	}

	/**
	 * Initiate OAuth handshake and redirect user to OAuth server to authorize
	 * the app.
	 */
	protected function handleInitiate() {
		list( $next, $token ) = $this->oauth->initiate();
		$_SESSION[self::REQEST_KEY] = "{$token->key}:{$token->secret}";
		$this->redirect( $next );
	}

	/**
	 * Process the return result from a user authorizing our app.
	 */
	protected function handleCallback() {

		$next = false;
		if ( isset( $_SESSION[AuthManager::NEXTPAGE_SESSION_KEY] ) ) {
			$next = $_SESSION[AuthManager::NEXTPAGE_SESSION_KEY];
			$next = filter_var( $next, \FILTER_VALIDATE_URL, \FILTER_FLAG_PATH_REQUIRED );
		}
		if ( !isset( $_SESSION[self::REQEST_KEY] ) ) {
			$this->flash( 'error', 'An error occured; please try logging in again.' );
			$this->redirect( $this->urlFor( 'home' ) );
		}
		list( $key, $secret ) = explode( ':', $_SESSION[self::REQEST_KEY] );
		unset( $_SESSION[self::REQEST_KEY] );
		$token = new Token( $key, $secret );
		$this->form->requireString( 'oauth_verifier' );
		$this->form->requireInArray( 'oauth_token', [ $key ] );
		if ( $this->form->validate( $_GET ) ) {
			$verifyCode = $this->form->get( 'oauth_verifier' );
			try {
				/** @var Token $accessToken */
				$accessToken = $this->oauth->complete( $token, $verifyCode );
				$_SESSION[self::ACCESS_TOKEN_KEY] = $accessToken->key;
				$_SESSION[self::ACCESS_TOKEN_SECRET] = $accessToken->secret;
				$ident = $this->oauth->identify( $accessToken );
				$this->flash( 'info', 'You are now successfully logged in as ' . $ident->username );
			} catch ( \Exception $e ) {
				$this->flash( 'error', 'Logging in attempt aborted. Error!' );
			}
			$this->redirect( $next ?: $this->urlFor( 'home' ) );
		} else {
			$this->flash( 'error', 'Unable to validate request parameters.' );
		}
		$this->redirect( $this->urlFor( 'home' ) );
	}
}
