<?php

namespace Wikisource\Pg2ws\Controllers;

use Wikimedia\Slimapp\Controller;
use Wikisource\Api\WikidataQuery;
use Wikisource\Api\WikisourceApi;

class HomeController extends Controller
{

	public function handleGet() {
		// Handle mis-directed Oauth callback
		// (because the wrong callback URL was registered on Meta).
		if ( $this->request()->get( 'oauth_verifier' ) ) {
			$this->redirect(
				$this->slim->urlFor( 'oauth_callback' )
				.'?oauth_verifier='.$this->request()->get( 'oauth_verifier' )
				.'&oauth_token='.$this->request()->get( 'oauth_token' )
			);
		}

		// Query Wikidata for all the items with both a Wikisource Index page and a PG ID.
		$query = "SELECT ?item ?itemLabel ?pgId ?commonsFile ?wikisourceIndex
			WHERE {
				?item wdt:P2034 ?pgId .
				?item wdt:P1957 ?wikisourceIndex .
				OPTIONAL { ?item wdt:P996 ?commonsFile } .
				SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' }
			} ORDER BY ASC(xsd:integer(?pgId)) ";
		$wd = new WikidataQuery( $query );
		$wsApi = new WikisourceApi();
		$wsApi->setCache( $this->cache );
		$books = $wd->fetch();
		foreach ( $books as $i => $b ) {
			// Q number.
			$wikidataPrefix = 'http://www.wikidata.org/entity/';
			$books[$i]['itemQnum'] = substr( $b['item'], strlen( $wikidataPrefix ) );
			// Commons file.
			if ( !empty( $b['commonsFile'] ) ) {
				$commonsPrefixLen = strlen( 'https://commons.wikimedia.org/wiki/Special:Filepath' );
				$books[$i]['commonsFilename'] = urldecode(
					substr( $b['commonsFile'], $commonsPrefixLen )
				);
			}
			// Index page title and quality.
			if ( !empty( $b['wikisourceIndex'] ) ) {
				$url = urldecode( $b['wikisourceIndex'] );
				$wikisource = $wsApi->newWikisourceFromUrl( $url );
				$indexPage = $wikisource->getIndexPageFromUrl( $url );
				$books[$i]['wikisourceIndexTitle'] = $indexPage->getTitle();
				$books[$i]['quality'] = $indexPage->getQuality();
			} else {
				$books[$i]['wikisourceIndexTitle'] = '';
				$books[$i]['quality'] = '';
			}
		}
		$this->view->set( 'books', $books );
		$this->render( 'index.html' );
	}

}
