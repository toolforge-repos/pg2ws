<?php

namespace Wikisource\Pg2ws\Controllers;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use MediaWiki\OAuthClient\Token;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Wikisource\Api\WikidataQuery;
use Wikisource\Api\IndexPage;
use Wikisource\Api\Wikisource;
use Wikisource\Api\WikisourceApi;

class TransferController extends \Wikimedia\Slimapp\Controller
{

	public function handleGet() {

		$wikisourceIndexUrl = $this->slim->request()->params( 'index' );
		$wikisourcePageNum = $this->slim->request()->params( 'page' );
		if ( is_null( $wikisourceIndexUrl ) ) {
			$this->flash( 'error', "No Index page URL provided." );
			$this->redirect( $this->urlFor( 'home' ) );
		}
		$this->outputEditingForm( $wikisourceIndexUrl, $wikisourcePageNum );
	}

	/**
	 * @param string $indexPageUrl
	 */
	protected function getIndexPage( $indexPageUrl ) {
		// Index page.
		$wsApi = new WikisourceApi();
		$wsApi->setCache( $this->cache );
		$wikisource = $wsApi->newWikisourceFromUrl( $indexPageUrl );
		return $wikisource->getIndexPageFromUrl( $indexPageUrl );
	}

	/**
	 * Output the side-by-side editing form and page image. Used in both GET and POST requests.
	 * @param string $wikisourceIndexUrl
	 * @param integer $wikisourcePageNum
	 */
	protected function outputEditingForm( $wikisourceIndexUrl, $wikisourcePageNum ) {

		$query = "SELECT ?item ?itemLabel ?pgId
            WHERE {
              ?item wdt:P2034 ?pgId .
              ?item wdt:P1957 <$wikisourceIndexUrl> .
              SERVICE wikibase:label { bd:serviceParam wikibase:language 'en' }
            }";
		$wd = new WikidataQuery( $query );
		$info = $wd->fetch();
		if ( !isset( $info[0] ) ) {
			$this->flash( 'error', "No info found for Index page: '$wikisourceIndexUrl'" );
			$this->redirect( $this->urlFor( 'home' ) );
		}
		$pgId = $info[0]['pgId'];
		$this->slim->view()->set( 'itemLabel', $info[0]['itemLabel'] );
		$this->slim->view()->set( 'pgId', $pgId );

		$indexPage = $this->getIndexPage( $wikisourceIndexUrl );

		$this->view->set( 'indexPage', $indexPage );

		// If a single page is being requested.
		$pageInfo = $indexPage->getChildPageInfo( $wikisourcePageNum, 'num' );
		if ( $pageInfo !== false ) {
			if ( !$this->user ) {
				$this->flashNow( 'warning', 'You are not logged in and will not be able to upload this page.' );
			}

			$this->view->set( 'page', $pageInfo );

			$img = $this->getPageImage( $pageInfo['url'] );
			$this->view->set( 'pageImg', $img );

			$pgText = $this->slim->request()->post( 'pgtxt', $this->getGutenbergText( $pgId ) );
			$this->view->set( 'pgtxt', $pgText );

		}

		$this->render( 'transfer.html' );
	}

	/**
	 * Given a URL to a Page namespace page on Wikisource, extract the page image.
	 *
	 * @param string $pageUrl
	 */
	protected function getPageImage( $pageUrl ) {

		$client = new Client();
		$response = $client->request( 'GET', $pageUrl );
		$pageContents = $response->getBody()->getContents();
		$pageCrawler = new Crawler;
		$pageCrawler->addHTMLContent( $pageContents, 'UTF-8' );
		$pageImage = $pageCrawler->filterXPath( "//div[@class='prp-page-image']//img" )->getNode( 0 );
		$pageImageSrc = $pageImage->getAttribute( 'src' );
		return $pageImageSrc;
	}

	/**
	 * Get PG text (HTML if it exists, or text otherwise). Randomly chooses a different mirror each time.
	 * @param int $pgId
	 * @throws \Exception If unable to get either the HTML or text versions.
	 */
	protected function getGutenbergText( $pgId ) {

		// Set up the Project Gutenberg mirror, from http://www.gutenberg.org/MIRRORS.ALL
		$mirrors = [
			//'http://www.gutenberg.org/dirs/', // Seems to be 403'ing intermittently.
			'http://www.gutenberg.lib.md.us/',
			'http://gutenberg.pglaf.org/',
			'http://gutenberg.readingroo.ms/',
		];
		$mirror = $mirrors[array_rand( $mirrors, 1 )];
		// Build mirror path. E.g. #19 => 1/19; #15453 => 1/5/4/5/15453.
		$pgIdStr = (string)$pgId;
		if ( $pgId < 100 ) {
			$path = $pgIdStr[0] . '/' . $pgIdStr;
		} else {
			$path = preg_replace( '/([0-9])/', '$1/', substr( $pgIdStr, 0, -1 ) ) . $pgIdStr;
		}
		$mirrorPath = $mirror . $path;

		// Try to get the HTML version.
		$client = new Client();
		$htmlUrl = "$mirrorPath/$pgId-h/$pgId-h.htm";
		$response = $client->request( 'GET', $htmlUrl );
		if ( $response->getStatusCode() !== 200 ) {
			// Fall back to txt.
			$txtUrl = "$mirrorPath/$pgIdStr.txt";
			$response = $client->request( 'GET', $txtUrl );
		}
		if ( $response->getStatusCode() !== 200 ) {
			throw new \Exception( "Unable to get text of Project Gutenberg ebook #$pgId" );
		}
		return $response->getBody()->getContents();
	}

	/**
	 * Handle the POST submission of the proofreading form.
	 */
	public function handlePost() {

		// User auth.
		if ( !$this->user ) {
			$this->redirect( $this->urlFor( 'home' ) );
		}
		$accessToken = new Token( $_SESSION[AuthController::ACCESS_TOKEN_KEY], $_SESSION[AuthController::ACCESS_TOKEN_SECRET] );

		// Get the form inputs.
		$wsTxt = $this->slim->request()->post( 'wstxt' );
		$pgTxt = $this->slim->request()->post( 'pgtxt' );
		$indexPageUrl = $this->slim->request()->post( 'index' );
		$pageNum = $this->slim->request()->post( 'page' );

		// Fetch the index page now so we can determine the API URL
		// (we want the page later anyway, so this isn't a waste).
		$indexPage = $this->getIndexPage( $indexPageUrl );

		// Get editing token.
		$apiUrl = $indexPage->getWikisource()->getMediawikiApi()->getApiUrl();
		$tokUrl = $apiUrl . '?action=tokens&format=json';
		$tokResponse = $this->slim->oauthClient->makeOAuthCall( $accessToken, $tokUrl );
		$editToken = json_decode( $tokResponse )->tokens->edittoken;

		// Edit the page.
		$pageInfo = $indexPage->getChildPageInfo( $pageNum, 'num' );
		$apiParams = [
			'action' => 'edit',
			'title' => $pageInfo['title'],
			'summary' => 'Import page from Project Gutenberg ebook.',
			'text' => $wsTxt,
			'token' => $editToken,
			'format' => 'json',
		];
		$this->oauthClient->setExtraParams( $apiParams );
		$editRespose = $this->oauthClient->makeOAuthCall( $accessToken, $apiUrl, true, $apiParams );
		$editResponse = json_decode( $editRespose );
		if ( isset( $editRespose->error ) ) {
			throw new \Exception( $editResponse->error->info );
		}

		// Pass back to normal GET request handler. Bit of a hack, this.
		$this->outputEditingForm( $indexPage->getUrl(), $pageNum + 1 );
	}
}
